﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TesteForms
{
	public class App : Application
	{
		public static List<string> PhoneNumbers { get; set; }
		public static List<string> Clientes { get; set; }

		public App ()
		{
			PhoneNumbers = new List<string> ();
			Clientes = new List<string>();
			var myNav = new NavigationPage(new TesteForms.MainPage()) {
				Tint = Color.White, 
				Icon = "AppIcon.png",
				BarTextColor = Color.Black

			};

			MainPage = myNav;

			for (int i = 0; i < 10; i++) {
				Clientes.Add ("Cliente " + i);
			}
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}