﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Timers;

namespace TesteForms
{
	public partial class CallHistoryPage : ContentPage
	{
		
		public List<Cliente> clientes;
		ListView lista;
		WebCalls webcalls = new WebCalls();
		ActivityIndicator a = new ActivityIndicator();
		public bool Loading { get; set;}

		public CallHistoryPage ()
		{
			InitializeComponent ();
			Global.GeneralConstructor (this);
			GetClientes ();
			//Draw ();

		}

		public async void Draw(){


			ActivityIndicator ai = new ActivityIndicator ();
			ai.IsRunning = true;
			ai.IsEnabled = true;
			ai.BindingContext = this;
			ai.SetBinding (ActivityIndicator.IsVisibleProperty, "Loading");
			Loading = true;
			clientes = new List<Cliente> ();
			//Create array for test
//			for (int i = 0; i < 15; i++) {
//				Cliente novo = new Cliente();
//				novo.Nome = "Cliente " + i;
//				novo.Activo = true;
//				clientes.Add (novo);
//			}


			//style the list
			lista = new ListView
			{
			};

			//define where the data comes from
			lista.ItemsSource = clientes;

			//style each cell using the Object properties of the class of the items on the list
			lista.ItemTemplate = new DataTemplate(typeof(Cliente));

			lista.ItemSelected += (sender,  e) => {
				Cliente todoItem = e.SelectedItem as Cliente;
				var todoPage = new DetailPage(todoItem); // so the new page shows correct data
				Navigation.PushAsync(todoPage);
			};


			Content = new StackLayout
			{
				VerticalOptions = LayoutOptions.FillAndExpand,

				Children = { lista,a }
			};


			//	GetClientes ();
				lista.ItemsSource = null;
				lista.ItemsSource = Global.Clientes.ListaAtual;

		}


		public async Task GetClientes(){
			
			bool result = await webcalls.CarregaClientes ("3", "true", "", "", "");
			lista.ItemsSource = null;
			lista.ItemsSource = Global.Clientes.ListaAtual;
			//	clientes = Global.Clientes.ListaAtual;
			//Loading = false;
		}


	
		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			Draw ();
			//GetClientes ();
		}
	
	}



}

