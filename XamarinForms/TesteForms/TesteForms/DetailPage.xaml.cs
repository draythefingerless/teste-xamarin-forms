﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TesteForms
{
	public partial class DetailPage : ContentPage
	{
		public DetailPage (Cliente c)
		{
			this.BindingContext = c;

			InitializeComponent ();
			Global.GeneralConstructor (this);

			Title = c.Nome;

			var firstLabel = new Label{
				HorizontalOptions= LayoutOptions.Fill,
				Text = "First Name: ",
				TextColor = Color.Black
			};
			var firstText = new Label{
				HorizontalOptions= LayoutOptions.Fill,
				Text = c.Nome,
				TextColor = Color.Black
			};
			var firstNameLayout = new StackLayout {
				Orientation = StackOrientation.Horizontal,	
				Children = {firstLabel, firstText}
			};



			var lastLabel = new Label {
				HorizontalOptions = LayoutOptions.Fill,
				Text = "Last Name: ",
				TextColor = Color.Black
			};
			var lastText = new Label {
				HorizontalOptions = LayoutOptions.Fill,
				Text = "PlaceHolder",
				TextColor = Color.Black
			};
			var lastNameLayout = new StackLayout {
				Orientation = StackOrientation.Horizontal,	
				Children = {lastLabel, lastText}
			};



			var activoLabel = new Label {
				HorizontalOptions = LayoutOptions.Fill,
				Text = "Activo: ",
				TextColor = Color.Black
			};
			var activoText = new Label {
				HorizontalOptions = LayoutOptions.Fill,
				Text = c.Activo.ToString(),
				TextColor = Color.Black
			};
			var activoLayout = new StackLayout {
				Orientation = StackOrientation.Horizontal,	
				Children = {activoLabel, activoText}
			};


			Content = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Children = {firstNameLayout, lastNameLayout, activoLayout}
			};
		}
	}
}

