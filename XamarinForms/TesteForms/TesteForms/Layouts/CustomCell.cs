﻿using System;
using Xamarin.Forms;

namespace TesteForms
{
	class ClienteCell : ViewCell
	{
		public ClienteCell()
		{
			var image = new Image
			{
				HorizontalOptions = LayoutOptions.Start
			};
			image.SetBinding(Image.SourceProperty, new Binding("ImageUri"));
			image.WidthRequest = image.HeightRequest = 40;

			var nameLayout = CreateNameLayout();

			var viewLayout = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { image, nameLayout }
			};
			View = viewLayout;
		}

		static StackLayout CreateNameLayout()
		{

			var nameLabel = new Label
			{
				HorizontalOptions= LayoutOptions.FillAndExpand
			};
			nameLabel.SetBinding(Label.TextProperty, "Nome");

			var activoLabel = new Label
			{
				HorizontalOptions= LayoutOptions.FillAndExpand
			};
			activoLabel.SetBinding(Label.TextProperty, "Activo");


			var nameLayout = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Orientation = StackOrientation.Vertical,
				Children = { nameLabel, activoLabel}
			};
			return nameLayout;
		}
	}
}

