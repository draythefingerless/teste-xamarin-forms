﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace TesteForms
{
	public class WebCalls
	{
		#region VARIAVEIS GLOBAIS

		#if DEBUG
			const string genericurl ="http://servicesdemo.lumarca.grupoma.eu/service.asmx/";
//			const string genericurl = @"http://services.lumarca.grupoma.eu/service.asmx/";
		#else
			const string genericurl = @"http://services.lumarca.grupoma.eu/service.asmx/";
		#endif


		const string hash="h=026dc19d489c4e4db73e28a71137bd7b";
		public static string Codvendedor = "0";
		public static string Idvendedor = "0";
		public static string NomeVendedor;
		public static bool IsMaster; 
		public static bool IsAdmin;
		public static Vendedor ListaVendedores;
		public static List<Produto> UnidadesNegocio;
		public static List<ClienteEstado> ClienteEstados;
		public static List<TipoPagamento> TiposPagamento;
		public static string[] VisitasResultados;

		public enum ResultadoLogin
		{
			AutenticacaoOk,
			AutenticacaoFalhou,
			VendedorInativo,
			ErroServidor
		}

		#endregion

		public WebCalls () {}

		public async Task<bool> CarregaClientes(string codVendedor, string all, string texto, string estado, string idRota)
		{
			try {
				string parametros = 
					"ClienteGetList?" +
					 hash +
					"&CodVendedor=" + codVendedor +
					"&all=" + all +
					"&texto=" + texto +
					"&estado=" + estado +
					"&idRota=" + idRota;

//				string url = 
//					genericurl + 
//					"EnviarErro?" + 
//					hash + 
//					parametros;

					var client = new System.Net.Http.HttpClient ();

					client.BaseAddress = new Uri(genericurl);

					var response = client.GetAsync(parametros).Result;

					string earthquakesJson = response.Content.ReadAsStringAsync().Result;
					//System.Diagnostics.Debug.WriteLine("coiso + "  + earthquakesJson);
					var rootobject = JsonConvert.DeserializeObject<Cliente>(earthquakesJson);					
					Global.Clientes.ListaAtual = rootobject.Clientes;
				return true;

			}
			catch(Exception e)
			{
				System.Diagnostics.Debug.WriteLine("object + "  + e);
				return false;
			}
		}
	}

}