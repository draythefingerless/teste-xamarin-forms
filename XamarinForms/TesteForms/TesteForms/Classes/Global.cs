﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using Xamarin.Forms;
using System.Linq;

namespace TesteForms
{
	public class Global 
	{
		WebCalls webCalls = new WebCalls ();

		public Global () { }


		public static string[] Meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
		public static string LocalizacaoAtualTexto = "";
		public static bool MostraCaminho = false;
		public static Page CurrentActivity;

		public static List<string> ClientesVistos = new List<string>();
		public static List<Produto> ProdutosCampanha = new List<Produto>();
		public static int campanhaIndex = 0;
		public static bool PotencialOuNormal = false;

		//check visitas on exit and entry
		public static bool AllVisitsClosed = true;
		public static bool checkingExit = false;
		public static bool checkingEntrance = false;

		//campos para fechar visitas num cliente com campos automaticos
		public static bool encomendou = false;
		public static bool liquidou = false;
		public static bool criouVisita = false;
		public static int encomendaOuOrcamento = 0;

		//lista e index para orcamentos expirados
		public static List<Orcamento> OrcamentosExpirados = new List<Orcamento>();
		public static int expiradosIndex = 0;

		//COMISSAO TABELA
		public static List<ComissaoLinha> tabela = new List<ComissaoLinha>();

		public enum Transicoes {
			Frente,
			Tras
		}

		public static AreaGlobal AreaAtual;
		public enum AreaGlobal {
			DashBoard,
			Clientes,
			Rotas,
			Catalogo,
			Encomendas,
			Orcamentos,
			Visitas,
			Mensagens,
			Ficheiros,
			Estatisticas,
			ClientesPotenciais
		}

		public enum JanelaAtual {
			ClientesDetalhes,
			ClientesEditar,
			ClientesPendentes,
			ClientesContaCorrente,
			ClientesEncomendasPendentes,
			ClientesGrafico,
			ClientesPreRecibos,
			ClientesCatalogoProdutos,
			ClientesCatalogoProdutosMaisVendidos,
			ClientesCatalogoProdutosSugestoes,
			ClientesCatalogoProdutoDetalhes,
			ClientesCarrinho,
			ClientesCarrinhoPreview,
			ClientesPedidosEncomenda,
			ClientesPedidosEncomendaDetalhes,
			ClientesPedidosOrcamento,
			ClientesPedidosOrcamentoDetalhes,
			ClientesVisitas,
			ClientesLocalizacao,
			Catalogo,
			CatalogoNovidades,
			CatalogoMaisVendidos,
			Condicoes,
			ProdutoDetalhes,
			FicheirosProdutos,
			FicheirosClientes,
			FicheirosGeral,
			Encomendas,
			EncomendaDetalhes,
			Orcamentos,
			OrcamentosDetalhes,
			Visitas,
			Rotas,
			RotasClientes,
			Mensagens,
			MensagemDetalhes,
			TabelaClientesPagamentosPendentes,
			TabelaVendasObjectivos,
			ClientesPotenciaisDetalhes,
			ClientesPotenciaisLocalizacao,
			ClientesPotenciaisEditar,
			ClientesPotenciaisVisitas
		}

		public static void ErroGeral(Page page, string erro = "", bool html = false) 
		{

		}

		public static void GeneralConstructor(Page p)
		{
			p.BackgroundColor = Color.FromRgb (240, 240, 240);
			FileImageSource x = "AppIcon.png";
			NavigationPage.SetTitleIcon (p, x);

		}
		public static void ToastMsg(Page page, string texto, bool erro)
		{
			
		}

		public static void VerificaOrcamentos(Page page, bool logout)
		{

		}

		public static void VerificaSituacaoVendedor(Page page, bool logout)
		{
			
		}

		public static void VerifyVisitas(Page x)
		{			
			
		}

		public static void Sair(Page page)
		{
			checkingExit = true;
		}

		public static void VendedorRegistar(Page page, string tipo, string km, string matricula, string data, string latitude, string longitude)
		{

		}
			


		public static class Clientes
		{
			public static string Id = "0";
			public static string Codigo = "";
			public static string Nome = "";
			public static string CodEstabelecimento = "";
			public static string CoordsGPS = "";
			public static string AdicionarVisita = "0";
			public static string Morada = "";
			public static string Email = "";
			public static bool TemVisita;
			public static bool Carrinho;
			public static bool CarregarTodos;
			public static string FiltroTexto = "";
			public static string FiltroEstado = "";
			public static int PosicaoAtual;
			public static List<Cliente> ListaAtual;
			public static Dictionary<string,int> AlphabetIndex = new Dictionary<string, int>();
			public static string[] Sections;
		}

		public static class ClientesPotenciais
		{
			public static string Id = "0";
			public static string Codigo = "";
			public static string Nome = "";
			public static string CodEstabelecimento = "";
			public static string CoordsGPS = "";
			public static string AdicionarVisita = "0";
			public static string Morada = "";
			public static string Email = "";
			public static bool TemVisita;
			public static bool Carrinho;
			public static bool CarregarTodos;
			public static string FiltroTexto = "";
			public static string FiltroEstado = "";
			public static int PosicaoAtual;
			public static List<ClientePotencial> ListaAtual;
			public static Dictionary<string,int> AlphabetIndex = new Dictionary<string, int>();
			public static string[] Sections;
		}

		public static class Produtos
		{
			public enum Areas
			{
				Catalogo,
				MaisVendidos,
				Novidades,
				Sugestoes
			}

			public static string CodAtual = "";
			public static string DesignacaoAtual = "";
			public static List<string> Caminho = new List<string>();
			public static string FiltroNivel = "";
			public static string FiltroParent = "";
			public static string FiltroTexto = "";
			public static string[] FiltroData = { "", "" };
			public static Areas AreaAtual = Areas.Catalogo;
		}

		public static class Ficheiros
		{
			public enum Areas
			{
				Geral,
				Produtos,
				Clientes
			}
			public static Areas AreaAtual = Areas.Geral;
		}

		public static class Encomendas
		{
			public static string FiltroCodVendedor = "";
			public static string FiltroNomeVendedor = "";
			public static string FiltroCodCliente = "";
			public static string FiltroNumEncomenda = "";
			public static string FiltroNomeCliente = "";
			public static string FiltroDataInicio = "";
			public static string FiltroDataFim = "";
			public static string FiltroMes = "0";
			public static string FiltroSemana = "0";
			public static string FiltroDia = "0";
		}

		public static class Condicoes
		{
			public static string FiltroCodVendedor = "";
			public static string FiltroNomeVendedor = "";
			public static string FiltroCodCliente = "";
			public static string FiltroNomeCliente = "";
			public static string FiltroDataFim = "";
			public static string FiltroMes = "0";
			public static string FiltroSemana = "0";
			public static string FiltroDia = "0";
			public static string Tipo = "";
		}

		public static class Orcamentos
		{
			public static string FiltroCodVendedor = "";
			public static string FiltroNomeVendedor = "";
			public static string FiltroCodCliente = "";
			public static string FiltroNumOrcamento = "";//CHANGE HERE
			public static string FiltroNomeCliente = "";
			public static string FiltroDataInicio = "";
			public static string FiltroDataFim = "";
			public static string FiltroMes = "0";
			public static string FiltroSemana = "0";
			public static string FiltroDia = "0";
		}

		public static class Visitas
		{
			public static string FiltroCodVendedor = "";
			public static string FiltroNomeVendedor = "";
			public static string FiltroCodCliente = "";
			public static string FiltroEstado = "";
			public static string FiltroNomeCliente = "";
			public static string CodCliente = "";
			public static string FiltroDataInicio = "";
			public static string FiltroDataFim = "";
			public static string FiltroMes = "0";
			public static string FiltroSemana = "0";
			public static string FiltroDia = "0";
			public static List<string> DialogVisitasResultados = new List<string>();
		}

		public static class Mensagens
		{
			public static string FiltroCodVendedor = "";
			public static string FiltroNomeVendedor = "";
			public static string FiltroDataInicio = "";
			public static string FiltroDataFim = "";
			public static string FiltroMes = "0";
			public static string FiltroSemana = "0";
			public static string FiltroDia = "0";
		}

		public static class Rotas
		{
			public static string FiltroCodVendedor = "0";
			public static string FiltroNomeVendedor = "";
		}

		public static class Estatisticas
		{
			public static string FiltroDataInicio = "";
			public static string FiltroDataFim = "";
			public static int PosicaoAtual;
			public static List<Cliente> ListaAtual;
			public static bool CarregarTodos;
			public static string FiltroTexto = "";
			public static string FiltroEstado = "";




		}
	}

	public static class DateTimeExtensions
	{
		public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
		{
			int diff = dt.DayOfWeek - startOfWeek;
			if (diff < 0)
			{
				diff += 7;
			}
			return dt.AddDays(-1 * diff).Date;
		}
	}
}