﻿using System;

namespace TesteForms
{
	public interface IDialer
	{
		bool Dial(string number);
	}
}