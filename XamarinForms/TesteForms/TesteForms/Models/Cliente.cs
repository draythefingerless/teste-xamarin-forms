﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TesteForms
{
	[Serializable]
	public class Cliente : ViewCell
	{
		public string Nid {get;set;}
		public string CodCliente {get;set;}
		public string CodEstabelecimento {get;set;}
		public string Nome {get;set;}
		public string NomeZona {get;set;}
		public string RespNome {get;set;}
		public string RespDepart {get;set;}
		public string RespTelef {get;set;}
		public string RespTelem {get;set;}
		public string RespFax {get;set;}
		public string RespEmail {get;set;}
		public string RespObs {get;set;}
		public string Morada {get;set;}
		public string CoordGPS {get;set;}
		public string CoordGoogle {get;set;}
		public string Localidade {get;set;}
		public string CodPostal {get;set;}
		public string Distrito {get;set;}
		public string Concelho {get;set;}
		public string Freguesia {get;set;}
		public string WebSite {get;set;}
		public string Email {get;set;}	
		public string Telefone {get;set;}
		public string Telemovel {get;set;}
		public string Fax {get;set;}
		public string NumContrib {get;set;}
		public string Obs {get;set;}
		public double DescontoBase {get;set;}
		public DateTime dataNascimento {get;set;}
		public bool Activo {get;set;}
		public bool Aniversario {get;set;}
		public int Anos {get;set;}
		public bool PagamentoPendente {get;set;}
		public string CondicoesFinanceiras {get;set;}
		public double PlafondAtribuido {get;set;}
		public string TotalUtilizado {get;set;}
		public string SeguroCredito {get;set;}
		public string StatusFinanceiroProblema {get;set;}
		public bool StatusFinanceiro {get;set;}
		public bool VisitaPendente { get; set;}
		public string Estado { get; set;}
		public string EstadoTexto { get; set;}
		public List<Cliente> Clientes {get;set;}

		public Cliente()
		{
			var image = new Image
			{
				HorizontalOptions = LayoutOptions.Start
			};
			image.SetBinding(Image.SourceProperty, new Binding("ImageUri"));
			image.WidthRequest = image.HeightRequest = 40;

			var nameLayout = CreateNameLayout();

			var viewLayout = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { image, nameLayout }
			};
			View = viewLayout;
		}

		static StackLayout CreateNameLayout()
		{

			var nameLabel = new Label
			{
				HorizontalOptions= LayoutOptions.FillAndExpand,
				TextColor = Color.Black
			};
			nameLabel.SetBinding(Label.TextProperty, "Nome");

			var activoLabel = new Label
			{
				HorizontalOptions= LayoutOptions.FillAndExpand,
				TextColor = Color.Black
			};
			activoLabel.SetBinding(Label.TextProperty, "Activo");


			var nameLayout = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Orientation = StackOrientation.Vertical,
				Children = { nameLabel, activoLabel}
			};
			return nameLayout;
		}
	}
}

