﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TesteForms
{
	public partial class MainPage : ContentPage
	{
		string translatedNumber;

		public MainPage ()
		{
			InitializeComponent ();
			Global.GeneralConstructor (this);
			DrawMenu ();
			this.Title = "Smart Sales";

		}

		void OnTranslate (object sender, EventArgs e)
		{
			translatedNumber = Core.PhonewordTranslator.ToNumber (phoneNumberText.Text);
			if (!string.IsNullOrWhiteSpace (translatedNumber)) {
				callButton.IsEnabled = true;
				callButton.Text = "Call " + translatedNumber;
			} else {
				callButton.IsEnabled = false;
				callButton.Text = "Call";
			}
		}

		async void OnCall (object sender, EventArgs e)
		{
			if (await this.DisplayAlert (
				"Dial a Number",
				"Would you like to call " + translatedNumber + "?",
				"Yes",
				"No")) {
				var dialer = DependencyService.Get<IDialer> ();
				if (dialer != null) {
					App.PhoneNumbers.Add (translatedNumber);
					callHistoryButton.IsEnabled = true;
					dialer.Dial (translatedNumber);
				}
			}
		}

		void OnCallHistory(object sender, EventArgs e)
		{
			Navigation.PushAsync (new CallHistoryPage ());
		}

		void DrawMenu()
		{
			Grid grid = new Grid
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,

				RowDefinitions = 
				{
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
				},
				ColumnDefinitions = 
				{
					new ColumnDefinition { Width = GridLength.Auto },
					new ColumnDefinition { Width = GridLength.Auto },
					new ColumnDefinition { Width = GridLength.Auto }
				}
			};

			grid.ColumnSpacing = 100;
			grid.RowSpacing = 100;



			MainMenuButton toClientes = new MainMenuButton("Clientes","dashboardMenuClientes",1);
			grid.Children.Add(toClientes, 0, 0);
			grid.Children.Add(new MainMenuButton("Encomendas","dashboardMenuEncomendas",2), 0, 1);
			grid.Children.Add(new MainMenuButton("Mensagens","dashboardMenuMensagens",3), 0, 2);
			grid.Children.Add(new MainMenuButton("Catálogo","dashboardMenuCatalogo",4), 1, 0);
			grid.Children.Add(new MainMenuButton("Visitas","dashboardMenuVisitas",5), 1, 1);
			grid.Children.Add(new MainMenuButton("Estatisticas","dashboardMenuEstatisticas",6), 1, 2);
			grid.Children.Add(new MainMenuButton("Rotas","dashboardMenuRotas",7), 2, 0);
			grid.Children.Add(new MainMenuButton("Ficheiros","dashboardMenuFicheiros",8), 2, 1);

			Content = grid;
		}


		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			DrawMenu ();
		}

		//Custom class view for the buttons in the dashboard
		public class MainMenuButton : ContentView
		{

			public event EventHandler Clicked;

			public void SendClicked () 
			{
				var handler = Clicked;
				if (handler != null)
					handler (this, EventArgs.Empty);
			}

			public MainMenuButton(string text,string imgPath, int pos)
			{
				VerticalOptions = LayoutOptions.FillAndExpand;
				HorizontalOptions = LayoutOptions.Fill;

				Button bt = new Button {
					HorizontalOptions = LayoutOptions.Center,
					BackgroundColor = Color.FromRgba(240,240,240,0)
				};
				bt.Image = imgPath + ".png";

				if(pos == 1)
					bt.Clicked += async (sender, e) => {
					bt.Image = imgPath + "Pressed.png";
					Navigation.PushAsync(new CallHistoryPage());

					};
				Label l = new Label{
					Text = text,
					TextColor = Color.Black,
					HorizontalOptions = LayoutOptions.Center,
					FontSize = 18
				};

				Content = new StackLayout{
					Orientation = StackOrientation.Vertical,
					VerticalOptions = LayoutOptions.StartAndExpand,
					HorizontalOptions = LayoutOptions.Center,
					Children = {bt,l}
				};

			}
		}
	}
}